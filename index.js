'use strict';
require('dotenv').config();
const TwitterStream = require('twitter-stream-api');
const Parse = require('./parse');

const keys = {
	consumer_key: process.env.KEY,
	consumer_secret: process.env.SECRET,
	token: process.env.TOKEN,
	token_secret: process.env.TOKENSECRET,
}

const Twitter = new TwitterStream(keys, false);
Twitter.stream('user', {
	with: 'followings'
});
Twitter.on('connection success', function (uri) {
    console.log('connection success', uri);
});
Twitter.on('connection error http', function (httpStatusCode) {
    console.log('connection error http', httpStatusCode);
});
Twitter.on('connection error unknown', function (error) {
    console.log('connection error unknown', error);
    Twitter.close();
});
// Twitter.on('data', function (obj) {
//     console.log('data', obj);
// });
Twitter.pipe(Parse).pipe(process.stdout);
