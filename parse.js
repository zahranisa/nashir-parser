const Transform = require('stream').Transform;
const tweets = require('./tweets.json');
const Metascraper = require('metascraper');
const {
    buildRules,
    del,
    post,
    snipString,
    getSelectors,
    scrape,
    truncate
} = require('./utils');
const DELETETYPE = 'DELETETYPE';
const FRIENDLIST = 'FRIENDLIST';
const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const TWEET = 'TWEET';
const UNKNOWN = 'UNKNOWN';
const SUMMARYMAX = 1200 - 3;
const SKIP = { skip: true };
const useTwoParagraphs = Object.assign({}, Metascraper.RULES, {
    description: [
        $ => $('meta[property="og:description"]').attr('content'),
        $ => $('meta[name="twitter:description"]').attr('content'),
        $ => $('meta[name="description"]').attr('content'),
        $ => $('meta[name="sailthru.description"]').attr('content'),
        $ => $('meta[itemprop="description"]').attr('content'),
        $ => $('.post-content p').slice(0, 2).text(),
        $ => $('.entry-content p').slice(0, 2).text(),
        $ => $('article p').slice(0, 2).text()
    ],
    videoFrame: [
        $ => $('iframe[src*=youtu]').attr('src')
    ]
});

const isYoutubeRegex = /^https?:\/\/(www\.)?(youtu.be|youtube.com\/watch)\//;
const isYoutubeEmbedRegex = /^https?:\/\/(www\.)?(youtu.be|youtube.com\/embed)\//;
const isFacebookVideoRegex = /^https?:\/\/(www.)?facebook\.com\/\S+?\/videos\/\d+?/;
const isRootDomainRegex = /https?:\/\/[^\/]+?\/?$/;

let selectorPromise = null;
let refreshSelectors = true;

const interval = setInterval(function(){
    refreshSelectors = true;
}, process.env.REFRESH);

// detect stream message type
const getMessageType = message => {
    if (message.event === 'unfollow') {
        return UNFOLLOW;
    }
    if (message.event === 'follow') {
        return FOLLOW;
    }
    if (message.created_at) {
        return TWEET;
    }
    if (message.delete && message.delete.status) {
        return DELETETYPE;
    }
    if (Array.isArray(message.friends) || Array.isArray(message.friends_str)) {
        return FRIENDLIST;
    }
    return UNKNOWN;
};

const transformTweet = tweet => {
    if (refreshSelectors || selectorPromise === null) {
        selectorPromise = getSelectors();
        refreshSelectors = false;
    }
    return selectorPromise.then(selectors => {
        return new Promise((resolve, reject) => {
            // skip retweets and quotes
            if (tweet.retweeted_status != null || tweet.is_quote_status) {
                resolve(SKIP);
                return;
            }
            // addedAt vs createdAt?
            if (tweet.user == null) {
                console.log('bad tweet', tweet);
            }
            const rval = {
                id: tweet.id_str,
                screenName: tweet.user.screen_name,
                addedAt: tweet.created_at,
                title: '',
                content: '',
                images: [],
                video: '',
                url: '',
                lang: ''
            };
            // build the title
            let snipArray = [];
            // handle retweets and quotes
            let curTweet = tweet.extended_tweet || tweet;
            const curText = curTweet.full_text || curTweet.text;
            rval.lang = curTweet.lang;
            // remove hashtags
            if (curTweet.entities.hashtags.length) {
                snipArray = snipArray.concat(
                    curTweet.entities.hashtags.map(hashtag => [
                        hashtag.indices[0],
                        hashtag.indices[0] + 1
                    ])
                );
            }
            // remove urls
            if (curTweet.entities.urls.length) {
                snipArray = snipArray.concat(
                    curTweet.entities.urls.map(url => url.indices)
                );
            }
            // remove media
            if (
                curTweet.entities.media != null &&
                curTweet.entities.media.length > 0
            ) {
                snipArray = snipArray.concat(
                    curTweet.entities.media.map(url => url.indices)
                );
            }
            // remove mentions
            if (
                curTweet.entities.user_mentions != null &&
                curTweet.entities.user_mentions.length > 0
            ) {
                snipArray = snipArray.concat(
                    curTweet.entities.user_mentions.map(mention => mention.indices)
                );
            }
            if (curTweet.display_text_range) {
                rval.title = snipString(
                    curText,
                    curTweet.display_text_range[0],
                    curTweet.display_text_range[1],
                    snipArray
                );
            } else {
                rval.title = snipString(curText, null, null, snipArray);
            }
            rval.title = rval.title.replace(/\\r|\\n|\n|↵/g, ' ') // remove extra newlines
            .replace(/(\.\s){2,}/g, ' ') // remove . . . patterns
            .replace(/\s{2,}/g, ' ') // remove extra spaces
            .replace(/_/g, ' ') // remove underscores
            .replace(/[V|v]ia\s*$/g, '') // remove via 
            .trim();

            // add media
            const entities = curTweet.extended_entities || curTweet.entities;
            if (entities.media != null && entities.media.length > 0) {
                const mediaArr = entities.media;
                for (var i = 0; i < mediaArr.length; i++) {
                    if (mediaArr[i].type === 'photo') {
                        rval.images.push(mediaArr[i].media_url_https);
                    } else if (
                        mediaArr[i].type === 'video' ||
                        mediaArr[i].type === 'animated_gif'
                    ) {
                        rval.video = mediaArr[i].video_info.variants[0].url;
                    } else {
                        console.log('unexpected type', mediaArr[i].type);
                    }
                }
            }

            let hasTitle = rval.title.trim() !== '';
            const hasUrls =
                curTweet.entities.urls != null &&
                curTweet.entities.urls.length > 0;
            // we take the first entity
            const curUrl = hasUrls && curTweet.entities.urls.map(u => u.expanded_url)[0];
            // build the content
            if (hasUrls && isRootDomainRegex.test(curUrl)) {
                if (hasTitle) {
                    resolve(rval);
                } else {
                    resolve(SKIP);
                }
            } else if (hasUrls && isYoutubeRegex.test(curUrl)) {
                if (hasTitle) {
                    rval.content = '';
                    rval.video = curUrl;
                    rval.url = '';
                    resolve(rval);
                } else {
                    resolve(SKIP);
                }
            } else if (hasUrls && isFacebookVideoRegex.test(curUrl)) {
                if (hasTitle) {
                    rval.content = '';
                    rval.video = `https://www.facebook.com/plugins/video.php?href=${encodeURIComponent(curUrl)}&width=720`;
                    rval.url = '';
                    resolve(rval);
                } else {
                    resolve(SKIP);
                }
            } else if (hasUrls) {
                rval.url = curUrl;
                let rules = useTwoParagraphs;
                if (selectors[rval.screenName] != null) {
                    rules = buildRules(selectors[rval.screenName]);
                }
                scrape(rval.url, rules)
                    .then(metadata => {
                        // set title if missing
                        if (metadata.title != null && !hasTitle) {
                            rval.title = truncate(metadata.title, SUMMARYMAX);
                            hasTitle = true;
                        }
                        // set the content
                        // first we check the description
                        if (
                            metadata.description &&
                            metadata.description.length > 0
                        ) {
                            rval.content = truncate(
                                metadata.description,
                                SUMMARYMAX
                            );
                        } else if (metadata.summary) {
                            // then we check the summary
                            rval.content = truncate(
                                metadata.summary,
                                SUMMARYMAX
                            );
                        } else if (metadata.title) {
                            // try the title
                            rval.content = truncate(metadata.title, SUMMARYMAX);
                        } else {
                            // fall back to existing title
                            rval.content = rval.title;
                        }
                        // set image if not set already
                        if (rval.images.length < 1 && metadata.image) {
                            rval.images.push(metadata.image);
                        }
                        if (metadata.videoFrame != null && rval.video === '' && isYoutubeEmbedRegex.test(metadata.videoFrame)) {
                            rval.video = metadata.videoFrame;
                        }
                        if (metadata.publisher === 'YouTube' && rval.video === '') {
                            rval.video = metadata.url;
                        }
                        // if we have a valid title we use it
                        if (hasTitle) {
                            rval.content = rval.content.trim();
                            resolve(rval);
                        } else {
                            // not a useful tweet
                            // skipping
                            resolve(SKIP);
                        }
                    })
                    .catch(e => {
                        // scrape errored
                        if (hasTitle) {
                            rval.content = curText.trim();
                            resolve(rval);
                        } else {
                            resolve(SKIP);
                        }
                    });
            } else {
                // no urls in the tweet
                if (hasTitle) {
                    rval.content = rval.title;
                    resolve(rval);
                } else {
                    resolve(SKIP);
                }
            }
        });
    });
};

// transformTweet(tweets.tweets[0]).then(t => { console.log('transformed: ', t)});
module.exports = new Transform({
    objectMode: true,
    transform(chunk, encoding, callback) {
        let message = null;
        try {
            message = JSON.parse(chunk.toString());
        } catch (e) {
            console.log('json parse error:', e);
        }
        if (message) {
            const type = getMessageType(message);
            switch (type) {
                case TWEET:
                    // console.log('got a tweet', message);
                    transformTweet(message)
                        .then(
                            item => {
                                if (item.skip !== true) {
                                    console.log(item);
                                    return post(item);
                                }
                            },
                            err => console.log('err', err)
                        )
                        .catch(err => console.log(err));
                    break;
                case FOLLOW:
                case UNFOLLOW:
                    refreshSelectors = true;
                    break;
                case DELETETYPE:
                    del(message.delete.status.id_str)
                    .then(() => { console.log('deleted message', message.delete.status.id_str)})
                    .catch(err => console.log(err));
                    break;
                default:
                    console.log('ignoring message of type', type);
            }
        }
        callback();
    }
});
