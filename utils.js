const http = require('http');
const Metascraper = require('metascraper');
const popsicle = require('popsicle');
const { decode } = require('iconv-lite');
const charset = require('charset');
// copied from https://github.com/andrewplummer/Sugar/blob/530a147e72ccd4adc83483564ba8788d7df7d438/lib/common.js
const TRUNC_REG = RegExp(
    '(?=[\u0009\u000A\u000B\u000C\u000D\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u2028\u2029\u3000\uFEFF])'
);
const ruleCache = {};

const UAs = [
    'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:50.0) Gecko/20100101 Firefox/50.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:49.0) Gecko/20100101 Firefox/49.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:48.0) Gecko/20100101 Firefox/48.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:48.0) Gecko/20100101 Firefox/48.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:47.0) Gecko/20100101 Firefox/47.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:46.0) Gecko/20100101 Firefox/46.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:46.0) Gecko/20100101 Firefox/46.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:45.0) Gecko/20100101 Firefox/45.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11; rv:45.0) Gecko/20100101 Firefox/45.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:43.0) Gecko/20100101 Firefox/43.0',
    'Mozilla/5.0 (Windows NT 10.0; rv:50.0) Gecko/20100101 Firefox/50.0',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:43.0) Gecko/20100101 Firefox/43.0',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.59 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36 Edge/13.10586',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10240',
    'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 10.0; Trident/7.0; rv:11.0) like Gecko',
    'Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko',
];

module.exports = {
    post: obj =>
        new Promise((resolve, reject) => {
            const data = JSON.stringify(obj);
            const options = {
                hostname: process.env.API_HOST,
                port: process.env.API_PORT,
                path: process.env.API_PATH,
                method: process.env.API_METHOD,
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(data)
                }
            };
            const req = http.request(options, res => {
                if (res.statusCode === 200) {
                    resolve(true);
                } else {
                    reject(
                        new Error(`unexpected status code ${res.statusCode}`)
                    );
                }
            });
            req.on('error', reject);
            req.write(data);
            req.end();
        }),
    del: id =>
        new Promise((resolve, reject) => {
            const data = JSON.stringify({ hashed_id: id });
            const options = {
                hostname: process.env.API_HOST,
                port: process.env.API_PORT,
                path: '/moayad/rss/twitterparserapi/tweet.json',
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(data)
                }
            };
            const req = http.request(options, res => {
                if (res.statusCode === 200) {
                    resolve(true);
                } else {
                    reject(
                        new Error(`unexpected status code ${res.statusCode}`)
                    );
                }
            });
            req.on('error', e => { reject(e); });
            req.write(data);
            req.end();
        }),
    getSelectors: () =>
        new Promise((resolve, reject) => {
            http
                .get(
                    `http://${process.env.API_HOST}:${process.env.API_PORT}/nashr/app/api/twitter_source`,
                    res => {
                        const { statusCode } = res;
                        const contentType = res.headers['content-type'];

                        let error;
                        if (statusCode !== 200) {
                            error = new Error(
                                `Request Failed.\n` +
                                    `Status Code: ${statusCode}`
                            );
                        } else if (!/^application\/json/.test(contentType)) {
                            error = new Error(
                                `Invalid content-type.\n` +
                                    `Expected application/json but received ${contentType}`
                            );
                        }
                        if (error) {
                            reject(error.message);
                            // consume response data to free up memory
                            res.resume();
                            return;
                        }

                        res.setEncoding('utf8');
                        let rawData = '';
                        res.on('data', chunk => {
                            rawData += chunk;
                        });
                        res.on('end', () => {
                            try {
                                const parsedData = JSON.parse(rawData);
                                const mappedData = parsedData.reduce(
                                    (prev, cur) => {
                                        if (cur.text_id_pars != null) {
                                            prev[cur.rss_url] =
                                                cur.text_id_pars;
                                        }
                                        return prev;
                                    },
                                    {}
                                );
                                resolve(mappedData);
                            } catch (e) {
                                reject(e.message);
                            }
                        });
                    }
                )
                .on('error', e => {
                    reject(`Got error: ${e.message}`);
                });
        }),
    // truncates a string to a certain length
    truncate: (str, max) => {
        if (str.length <= max) {
            // no truncation needed
            return str;
        }
        const words = str.split(TRUNC_REG);
        let count = 0;
        return `${words
            .filter(word => {
                count += word.length;
                return count <= max;
            })
            .join('')}...`;
    },
    // snip certain ranges from a string
    snipString: (str, start, end, indexArray) => {
        let rval = '';
        const sortedIndexArray = indexArray
            .slice(0)
            .sort((a, b) => a[0] - b[0]);
        let startPointer = start != null ? start : 0;
        let endPointer = end != null ? end : str.length;
        for (let index = 0; index < sortedIndexArray.length; index++) {
            if (sortedIndexArray[index][0] > startPointer) {
                rval += str.substring(startPointer, sortedIndexArray[index][0]);
            }
            startPointer = sortedIndexArray[index][1];
        }
        if (startPointer < endPointer) {
            rval += str.substring(startPointer, endPointer);
        }
        return rval.trim();
    },
    buildRules: selector => {
        if (ruleCache[selector] === undefined) {
            ruleCache[selector] = Object.assign({}, Metascraper.RULES, {
                description: [
                    $ => $(selector).slice(0, 2).text(),
                    $ => $('meta[property="og:description"]').attr('content'),
                    $ => $('meta[name="twitter:description"]').attr('content'),
                    $ => $('meta[name="description"]').attr('content'),
                    $ => $('meta[name="sailthru.description"]').attr('content'),
                    $ => $('meta[itemprop="description"]').attr('content'),
                    $ => $('.post-content p').slice(0, 2).text(),
                    $ => $('.entry-content p').slice(0, 2).text(),
                    $ => $('article p').slice(0, 2).text()
                ],
                videoFrame: [$ => $('iframe[src*=youtu]').attr('src')]
            });
        }
        return ruleCache[selector];
    },
    scrape: (url, rules) => {
        return popsicle.request({
            url: url,
            options: {
                jar: popsicle.jar()
            },
            headers: {
                'User-Agent': UAs[Math.floor(Math.random() * UAs.length)],
            },
            transport: popsicle.createTransport({
                type: 'buffer'
            })
        })
        .then(response => {
            const encoding = charset(response.headers, response.body);
            const buffer = decode(response.body, encoding);
            return Metascraper.scrapeHtml(buffer.toString(), rules);
        });
    },
};
